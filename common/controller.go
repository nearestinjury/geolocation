package common

import (
	"encoding/json"
	"log"
	"net/http"

	"bitbucket.org/nearestinjury/geolocation/configuration"
	"bitbucket.org/nearestinjury/geolocation/database"
	"bitbucket.org/nearestinjury/geolocation/model"
	"github.com/gorilla/context"
	"github.com/gorilla/schema"
)

//Controller base controller
type Controller struct {
	Config configuration.Config
}

// Connection returns a connection to mongodb from the Request context
func (Controller) Connection(r *http.Request) *database.MongoDbConnection {
	if conn := context.Get(r, "connection"); conn != nil {
		return conn.(*database.MongoDbConnection)
	}

	log.Println("No mongodb connection in http context")
	return nil
}

// DecodeQuery decodes query parmameters to struct model
func (Controller) DecodeQuery(r *http.Request, value interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}

	return schema.NewDecoder().Decode(value, r.Form)
}

// DecodeJSON decodes json to struct model
func (Controller) DecodeJSON(r *http.Request, value interface{}) error {
	return json.NewDecoder(r.Body).Decode(value)
}

// Encode encodes a struct model to json
func (Controller) Encode(w http.ResponseWriter, value interface{}) error {
	return json.NewEncoder(w).Encode(value)
}

//ResponseSuccess send success response
func (c Controller) ResponseSuccess(w http.ResponseWriter, v interface{}) {
	w.WriteHeader(http.StatusOK)
	c.Encode(w, &model.ResponseModel{Data: v, Success: true})
}

//ResponseError send error response
func (c Controller) ResponseError(w http.ResponseWriter, v interface{}, responseCode int) {
	w.WriteHeader(responseCode)
	c.Encode(w, &model.ResponseModel{Error: v, Success: false})
}

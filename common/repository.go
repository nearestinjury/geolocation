package common

import (
	"bitbucket.org/nearestinjury/geolocation/database"
	"gopkg.in/mgo.v2"
)

//Repository struct
type Repository struct {
	Connection     *database.MongoDbConnection
	CollectionName string
}

//Collection returns a mongoDb collection
func (r Repository) Collection() *mgo.Collection {
	return r.Connection.Session.DB(r.Connection.Config.MongoDb.Database).C(r.CollectionName)
}

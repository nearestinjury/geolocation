package repository

import (
	"bitbucket.org/nearestinjury/geolocation/common"
	"bitbucket.org/nearestinjury/geolocation/database"
	"bitbucket.org/nearestinjury/geolocation/database/entities"
	"bitbucket.org/nearestinjury/geolocation/model"
	"gopkg.in/mgo.v2/bson"
)

//GeoRepository repository for handling geo loaction data
type GeoRepository struct {
	common.Repository
}

//NewGeoRepository returns an instance of NewGeoRepository
func NewGeoRepository(connection *database.MongoDbConnection) *GeoRepository {
	return &GeoRepository{common.Repository{
		Connection:     connection,
		CollectionName: "locations",
	}}
}

//Insert insert a location point
func (r GeoRepository) Insert(model model.GeoPointInsertModel) error {
	return r.Collection().Insert(&entities.GeoPointEntity{
		Name: model.Name,
		Tags: model.Tags,
		Location: entities.LocationEntity{
			Type:        "Point",
			Coordinates: []float64{model.Longitude, model.Latitude},
		},
	})
}

//Find get locations by max distance
func (r GeoRepository) Find(model model.GeoPointFindModel) ([]entities.GeoPointEntity, error) {
	results := []entities.GeoPointEntity{}
	err := r.Collection().Find(bson.M{
		"location": bson.M{
			"$near": bson.M{
				"$geometry": bson.M{
					"type":        "Point",
					"coordinates": []float64{model.Longitude, model.Latitude},
				},
				"$maxDistance": model.MaxDistance,
			},
		},
	}).All(&results)

	return results, err
}

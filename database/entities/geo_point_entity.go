package entities

import "gopkg.in/mgo.v2/bson"

//GeoPointEntity an geo location point
type GeoPointEntity struct {
	ID       bson.ObjectId  `json:"id" bson:"_id,omitempty"`
	Name     string         `json:"name" bson:"name"`
	Tags     []string       `json:"tags" bson:"tags"`
	Location LocationEntity `json:"location" bson:"location"`
}

package entities

//LocationEntity entity to handle location coordinates
type LocationEntity struct {
	Type        string    `json:"-"`
	Coordinates []float64 `json:"coordinates"`
}

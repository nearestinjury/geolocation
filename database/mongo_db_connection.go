package database

import (
	"time"

	"bitbucket.org/nearestinjury/geolocation/configuration"

	mgo "gopkg.in/mgo.v2"
)

// MongoDbConnection struct
type MongoDbConnection struct {
	Config  configuration.Config
	Session *mgo.Session
}

// NewMongoDbConnection returns an instance of MongoDbConnection
func NewMongoDbConnection(config configuration.Config) *MongoDbConnection {
	return &MongoDbConnection{Config: config}
}

// Open opens a connections against mongodb
func (m *MongoDbConnection) Open() error {
	session, err := mgo.DialWithInfo(&mgo.DialInfo{
		Addrs:    m.Config.MongoDb.Address,
		Database: m.Config.MongoDb.Database,
		Username: m.Config.MongoDb.Username,
		Password: m.Config.MongoDb.Password,
		Timeout:  m.Config.MongoDb.TimeoutSeconds * time.Second,
	})

	if err != nil {
		return err
	}

	session.SetMode(mgo.Monotonic, true)
	m.Session = session

	return nil
}

// Close closes the connection to mongodb
func (m MongoDbConnection) Close() {
	m.Session.Close()
}

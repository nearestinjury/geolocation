package model

//ResponseModel model
type ResponseModel struct {
	Data    interface{} `json:"data,omitempty"`
	Error   interface{} `json:"error,omitempty"`
	Success bool        `json:"status,omitempty"`
}

package model

//GeoPointInsertModel a model for geo location
type GeoPointInsertModel struct {
	Name      string   `json:"name"`
	Tags      []string `json:"tags"`
	Latitude  float64  `json:"lat"`
	Longitude float64  `json:"long"`
}

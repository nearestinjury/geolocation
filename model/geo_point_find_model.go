package model

//GeoPointFindModel model
type GeoPointFindModel struct {
	Latitude    float64 `json:"lat" schema:"lat"`
	Longitude   float64 `json:"long" schema:"long"`
	MaxDistance int     `json:"max_distance" schema:"max_distance"`
}

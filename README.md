# Nearest Injury - GeoLocation Webservice

Webservice for storing and retriving geo coordinates.

#### Routes
```sh
get - api/v1/locations?lat=53.123123&long=14.12345&max_distance=200
{
    "lat": float,
    "long": float,
    "max_distance": integer (meters)
}

post - api/v1/locations
{
    "name": string,
    "tags": string[]
    "lat": float,
    "long": float
}
```
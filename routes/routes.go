package routes

import (
	"bitbucket.org/nearestinjury/geolocation/configuration"
	"bitbucket.org/nearestinjury/geolocation/controller"
	"bitbucket.org/nearestinjury/geolocation/middleware"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
)

// Init initalizes the routes
func Init(config configuration.Config) *mux.Router {
	commonHandler := alice.New(
		middleware.NewLogMiddleware().Handler,
		middleware.NewJSONHeaderMiddleware().Handler,
		middleware.NewMongoDbMiddleware(config).Handler,
	)

	geoController := controller.NewGeoController(config)

	router := mux.NewRouter()
	subRouter := router.PathPrefix("/api/v1/").Subrouter()
	subRouter.Handle("/locations", commonHandler.ThenFunc(geoController.Find)).Methods("GET")
	subRouter.Handle("/locations", commonHandler.ThenFunc(geoController.Create)).Methods("POST")

	return router
}

package main

import (
	"log"
	"net/http"

	"bitbucket.org/nearestinjury/geolocation/configuration"
	"bitbucket.org/nearestinjury/geolocation/routes"
)

func main() {
	config := configuration.New()

	http.Handle("/", routes.Init(config))
	log.Fatal(http.ListenAndServe(":"+config.Port, nil))
}

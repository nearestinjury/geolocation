package controller

import (
	"log"
	"net/http"

	"bitbucket.org/nearestinjury/geolocation/common"
	"bitbucket.org/nearestinjury/geolocation/configuration"
	"bitbucket.org/nearestinjury/geolocation/database/repository"
	"bitbucket.org/nearestinjury/geolocation/model"
)

//GeoController struct
type GeoController struct {
	common.Controller
}

//NewGeoController controller to handle geo loaction
func NewGeoController(config configuration.Config) *GeoController {
	return &GeoController{common.Controller{Config: config}}
}

//Create add new geo location
func (c GeoController) Create(w http.ResponseWriter, r *http.Request) {
	model := &model.GeoPointInsertModel{}
	if err := c.DecodeJSON(r, model); err != nil {
		log.Println("Decode geo point error:", err)
		c.ResponseError(w, nil, http.StatusInternalServerError)
		return
	}

	if err := repository.NewGeoRepository(c.Connection(r)).Insert(*model); err != nil {
		log.Println("Insert geo point error:", err)
		c.ResponseError(w, nil, http.StatusInternalServerError)
		return
	}
}

//Find find a geo location by cooridnates and distance
func (c GeoController) Find(w http.ResponseWriter, r *http.Request) {
	model := &model.GeoPointFindModel{}
	if err := c.DecodeQuery(r, model); err != nil {
		log.Println("Decode geo point error:", err)
		c.ResponseError(w, nil, http.StatusInternalServerError)
		return
	}

	results, err := repository.NewGeoRepository(c.Connection(r)).Find(*model)
	if err != nil {
		log.Println("Find geo points error:", err)
		c.ResponseError(w, nil, http.StatusInternalServerError)
		return
	}

	c.ResponseSuccess(w, results)
}

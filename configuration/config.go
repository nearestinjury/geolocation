package configuration

import (
	"io/ioutil"
	"time"

	yaml "gopkg.in/yaml.v2"
)

// Config maps the fields and values in server.yml file
type Config struct {
	Port    string  `yaml:"port"`
	MongoDb MongoDb `yaml:"mongo_db"`
}

// MongoDb configuration mapping
type MongoDb struct {
	Database       string        `yaml:"database"`
	Username       string        `yaml:"username"`
	Password       string        `yaml:"password"`
	Address        []string      `yaml:"address"`
	TimeoutSeconds time.Duration `yaml:"timeout_seconds"`
}

// New returns a mapped struct with all the values from the server.yml file
func New() Config {
	source, err := ioutil.ReadFile("configuration/config.yml")
	if err != nil {
		panic(err)
	}

	var config Config
	err = yaml.Unmarshal(source, &config)
	if err != nil {
		panic(err)
	}

	return config
}

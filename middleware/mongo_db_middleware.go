package middleware

import (
	"log"
	"net/http"

	"bitbucket.org/nearestinjury/geolocation/configuration"
	"bitbucket.org/nearestinjury/geolocation/database"
	"github.com/gorilla/context"
)

// MongoDbMiddleware struct
type MongoDbMiddleware struct {
	config configuration.Config
}

// NewMongoDbMiddleware returns an instance of MongoDbMiddleware
func NewMongoDbMiddleware(config configuration.Config) *MongoDbMiddleware {
	return &MongoDbMiddleware{config}
}

// Handler the middleware handler
func (m *MongoDbMiddleware) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		connection := database.NewMongoDbConnection(m.config)
		err := connection.Open()
		if err != nil {
			log.Println("Database error:", err)
			return
		}
		defer connection.Close()

		context.Set(r, "connection", connection)
		next.ServeHTTP(w, r)
	})
}

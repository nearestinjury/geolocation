package middleware

import (
	"net/http"
)

// JSONHeaderMiddleware struct
type JSONHeaderMiddleware struct{}

// NewJSONHeaderMiddleware returns an instance of JSONHeaderMiddleware
func NewJSONHeaderMiddleware() *JSONHeaderMiddleware {
	return &JSONHeaderMiddleware{}
}

// Handler the middleware handler
func (JSONHeaderMiddleware) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}

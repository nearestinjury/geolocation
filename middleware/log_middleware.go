package middleware

import (
	"log"
	"net/http"
	"time"
)

// LogMiddleware struct
type LogMiddleware struct{}

// NewLogMiddleware returns an instance of LogMiddleware
func NewLogMiddleware() *LogMiddleware {
	return &LogMiddleware{}
}

// Handler the middleware handler
func (LogMiddleware) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()
		next.ServeHTTP(w, r)
		endTime := time.Now()
		log.Printf("[%s] %q %v\n", r.Method, r.URL.String(), endTime.Sub(startTime))
	})
}
